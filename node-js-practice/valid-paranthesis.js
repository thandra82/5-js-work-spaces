const firstString = '()[]{}'
const secondString = '([{}])'
const thirdString = ')(}{'

const parenthesisKeyMap = {
    '(': ')',
    '[': ']',
    '{': '}'
}


const validParenthesisString = (stringToCheck) => {
    const charArray = stringToCheck.split('')

    let interMediateArray = [];

    charArray.forEach((char) => {

        if (Object.keys(parenthesisKeyMap).includes(char)) {
            interMediateArray.push(char)
        } else if (Object.values(parenthesisKeyMap).includes(char)) {
            if (interMediateArray.length > 0 && parenthesisKeyMap[interMediateArray[interMediateArray.length - 1]] === char) {
                interMediateArray.pop(char)
            } else {
                return false
            }
        }


    })

    console.log(interMediateArray)
    return interMediateArray.length === 0
}

console.log(validParenthesisString(firstString))
console.log(validParenthesisString(secondString))
console.log(validParenthesisString(thirdString))